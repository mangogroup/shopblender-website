# Shopblender.nl

This website will be released at shopblender.nl.

#### Stylesheets

###### Boostrap & FontAwesome
To 'parse' the .less files you need to have less <= 1.7.5 installed globaly. If you do not have this run
```sh
$ npm install -g less@1.7.5
```

When you have installed less you can use the assetic:dump command. This will compile bootstrap v3 with custom variables.
```sh
$ php app/console assetic:dump
```

###### Custom stylesheets
For custom .scss files (/assets/stylesheets) run the composer watch command:
```sh
$ compass watch ./src/Mango/SiteBundle/Resources/assets/
```

Make sure you have installed the web assets with a symbolic link
```sh
$ php app/console assets:install web --symlink
```


### License
This product is part of the Mangogroup.

