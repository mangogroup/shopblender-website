<?php
namespace Mango\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PageController
 * @package Mango\SiteBundle\Controller
 */
class PageController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        // If this request is an ajax request
        if($request->request->get('isAjaxRequest')) {
            return $this->forward('MangoSiteBundle:Register:ajaxRegister', array(
                'request' => $request,
                'container' => $this->container
            ));
        }

        /** @var RegisterController $register */
        $register = new RegisterController();

        $registerFormBanner = $register->getForm($request, $this->container);
        $RegisterFormMiddle = $register->getForm($request, $this->container);

        return $this->render('MangoSiteBundle:Pages:index.html.twig', array(
            'registerFormBanner' => $registerFormBanner->createView(),
            'registerFormMiddle' => $RegisterFormMiddle->createView()
        ));
    }
}
