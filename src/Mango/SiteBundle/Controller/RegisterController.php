<?php
namespace Mango\SiteBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ODM\PHPCR\Query\Builder\QueryBuilder;
use Mango\Component\Core\Model\Application;
use Mango\Component\Core\Model\User;
use Mango\Component\Core\Model\Workspace;
use Mango\SiteBundle\Form\Type\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegisterController
 * @package Mango\SiteBundle\Controller
 */
class RegisterController extends Controller
{
    /**
     * @param Request $request
     * @param ContainerInterface $container
     * @return Response
     */
    public function ajaxRegisterAction(Request $request, ContainerInterface $container = null)
    {
        if($container) {
            $this->container = $container;
        }

        $registerForm = $this->getForm($request);

        $response = new Response();
        $response->headers->set('content-type', 'application/json');

        if($registerForm->isValid()) {
            $response->setStatusCode(Response::HTTP_CREATED);
            $content = array(
                'status' => 'ok',
                'message' => 'Uw webwinkel is nu online.',
                'url' => 'test.shopblender.nl'
            );
        } else {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $content = array(
                'errors' => $this->container->get('forminfo')->getFieldErrors($registerForm)
            );
        }

        $response->setContent(json_encode($content));

        return $response;
    }

    /**
     * @param Request $request
     * @param ContainerInterface $container
     * @return \Symfony\Component\Form\Form|FormInterface
     */
    public function getForm(Request $request, ContainerInterface $container = null)
    {
        if($container) {
            $this->container = $container;
        }

        /** @var User $user */
        $user = $this->container->get('mango.repository.user')->createNew();

        /** @var Application $application */
        $application = $this->container->get('mango.repository.application')->createNew();
        $application->addUser($user);

        $form = $this->container->get('form.factory')->createNamed('application', new RegistrationType(), $application);

        $form->handleRequest($request);

        if(true === $this->checkIfApplicationExists($application)) {
            $error = new FormError('Deze naam is al in gebruik');
            $form->get('name')->addError($error);
        }

        if ($form->isValid()) {
            /** @var Workspace $workspace */
            $workspace = $this->container->get('mango.repository.workspace')->createNew();
            $workspace->setName(uniqid());

            $users = $application->getUsers();
            foreach($users as $user) {
                $user->setWorkspace($workspace);
                $user->addRole('ROLE_MANGO_ADMIN');
                $user->setEnabled(true);
                $user->setApplicationRegistered($application);
                $user->setFirstName('');
                $user->setLastName('');
            }

            /** @var ObjectManager $manager */
            $manager = $this->container->get('mango.manager.application');

            $application->setWorkspace($workspace);
            $application->setDescription('');
            $application->setUrl($application->getName());

            $this->dispatchEvent('pre_create', 'application', $application);

            $manager->persist($application);
            $manager->flush();

            $this->dispatchEvent('post_create', 'application', $application);
        }

        return $form;
    }

    /**
     * @param Application $application
     * @return bool
     */
    public function checkIfApplicationExists(Application $application)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->container->get('mango.repository.application')->createQueryBuilder('a');
        $qb->select('count(a)')
            ->where('a.name = :name')->setParameter('name', $application->getName())
            ->orWhere('a.url LIKE :url')->setParameter('url', $application->getName() . '.%');
        $result = $qb->getQuery()->getSingleScalarResult();

        return $result ? true : false;
    }

    /**
     * @param $name
     * @param $resource
     * @param $subject
     */
    protected function dispatchEvent($name, $resource, $subject)
    {
        $this->get('event_dispatcher')->dispatch('mango.'.$resource.'.'.$name, new GenericEvent($subject));
    }
}
