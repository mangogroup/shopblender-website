<?php
namespace Mango\SiteBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class LocaleListener
 * @package Mango\SiteBundle\EventListener
 */
class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;

    /**
     * @param string $defaultLocale
     */
    public function __construct($defaultLocale = 'nl')
    {
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * Check if previous session is set,
     * otherwise set default language code
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if(!$request->hasPreviousSession()) {
            return;
        }

        if($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            $request->setLocale(
                $request->getSession()->get('_locale', $request->getPreferredLanguage(
                    array($this->defaultLocale, 'nl', 'de', 'be'))
                )
            );
        }
    }

    /**
     * LocaleListener gets called before the default locale listener
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(array('onKernelRequest', 17))
        );
    }
}