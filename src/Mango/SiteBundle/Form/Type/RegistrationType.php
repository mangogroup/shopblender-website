<?php

namespace Mango\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class RegistrationType
 * @package Mango\SiteBundle\Form\Type
 */
class RegistrationType extends AbstractType
{
    protected $dataClass;
    protected $property;

    public function __contruct($dataClass, $property)
    {
        $this->dataClass = $dataClass;
        $this->property = $property;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** Add webshop (application) name */
        $builder->add('name');

        $builder->add('users', 'collection', array(
            'type' => 'mango_user_profile',
            'allow_add' => false,
            'allow_delete' => false
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => $this->dataClass,
            'property' => $this->property
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mango_webshop_registration';
    }
}