<?php
namespace Mango\SiteBundle\Utils;

class FormInfo
{
    /**
     * @param \Symfony\Component\Form\Form $form
     * @return Array
     */
    public function getFieldErrors($form)
    {
        $errors = array();

        // get the form errors
        foreach($form->getErrors() as $err)
        {
            // check if form is a root
            if($form->isRoot())
                $errors['__GLOBAL__'][] = $err->getMessage();
            else
                $errors[] = $err->getMessage();
        }

        // check if form has any children
        if($form->count() > 0)
        {
            // get errors from form child
            foreach ($form->getIterator() as $key => $child)
            {
                if($child_err = $this->getFieldErrors($child))
                    $errors[$key] = $child_err;
            }
        }

        return $errors;
    }
}